import React, { Component } from "react";
import { Col, Button } from "react-bootstrap";
import history from "../History";
import axios from "axios";
import Form from "react-bootstrap/Form";

export default class LoginPopup extends Component {
  constructor() {
    super();
    this.state = {
      email: "",
      password: "",
    }
    axios.defaults.baseURL = 'http://localhost:8080';
    this.handleChange = this.handleChange.bind(this);
    this.loginUser = this.loginUser.bind(this);
  }

  handleChange(event) {
    const { name, value } = event.target;
    this.setState({ [name]: value });
  }


  redirect(role) {
    if (role.includes('ADMIN') || role.includes('EMPLOYEE')) {
      history.push("/panel")
    } else {
      history.push("/")
    }
    window.location.reload();
  }


  loginUser() {
    const config = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      proxy: {
        host: "localhost",
        port: 8080,
      }
    };
    const params = new URLSearchParams()
    params.append('email', this.state.email)
    params.append('password', this.state.password)
    axios
      .post("/login", params, config
      ).then((res) => {
        window.sessionStorage.setItem("email", res.data.email);
        window.sessionStorage.setItem("jwt", res.data.accessToken);
        window.sessionStorage.setItem("jwt_refresh", res.data.refreshToken);
        window.sessionStorage.setItem("role", res.data.role);
        window.sessionStorage.setItem("userId", res.data.userId);
        this.redirect(res.data.role)
        console.log(res.data)
      })
      .catch((e) => {
        alert(e)
      });
  }

  render() {
    return (
      <div>
        <Form>
          <Form.Group className="mb-3" controlId="formBasicEmail" >
            <Form.Label>Email address</Form.Label>
            <Form.Control type="email" placeholder="Email" name="email" onChange={this.handleChange} />
          </Form.Group>
          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control type="password" name="password" placeholder="Password" onChange={this.handleChange} />
          </Form.Group>
          <Col xs={{ span: 4, offset: 4 }} lg={{ span: 2, offset: 5 }}>
            <Button block variant="primary" onClick={this.loginUser}>
              Submit
            </Button>
          </Col>
        </Form>
      </div>
    );
  }

}