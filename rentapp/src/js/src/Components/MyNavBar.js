import React, { Component } from "react";
import { Navbar } from "react-bootstrap";
import { Nav } from "react-bootstrap"
import { Container } from "react-bootstrap";
import { Button, Modal } from "react-bootstrap";
import LoginPopup from "./LoginPopup";
import RegisterPopup from "./RegisterPopup";
import history from "../History";

export default class MyNavBar extends Component {
    constructor() {
        super();
        this.state = {
            modalShow: false,
            modalRegiserShow: false,
        };
    }

    setModalShow(value) {
        this.setState({ modalShow: value })
    }

    setModalRegisterShow(value) {
        this.setState({ modalRegiserShow: value })
    }

    logout() {
        window.sessionStorage.removeItem("email");
        window.sessionStorage.removeItem("jwt");
        window.sessionStorage.removeItem("jwt_refresh");
        window.sessionStorage.removeItem("role");
        history.push("/")
        window.location.reload();

    }

    renderButtons() {
        console.log(window.sessionStorage.getItem("role"))
        if (window.sessionStorage.getItem("email") != null) {
            return (
                <Nav>
                    <Nav.Link href="/user-rents">My rents</Nav.Link>
                    <Nav.Link href="/user-account">My account</Nav.Link>
                    <Button onClick={() => this.logout()}>
                        Logout
                    </Button>
                </Nav>
            )
        }
        else {
            return (
                <Nav>
                    <Button onClick={() => this.setModalShow(true)}>
                        Login
                    </Button>
                    " "
                    <Button onClick={() => this.setModalRegisterShow(true)}>
                        Register
                    </Button>
                </Nav>
            )
        }
    }

    render() {
        return (
            <>
                <Navbar bg="dark" variant="dark">
                    <Container>
                        <Navbar.Brand href="/">CarRental</Navbar.Brand>
                        <Nav className="me-auto">

                        </Nav>
                        {this.renderButtons()}
                    </Container>
                </Navbar>
                <RegisterModal
                    show={this.state.modalRegiserShow}
                    onHide={() => this.setModalRegisterShow(false)}
                />
                <LoginModal
                    show={this.state.modalShow}
                    onHide={() => this.setModalShow(false)}
                />
                <br />
            </>
        )
    }
}

function LoginModal(props) {
    return (
        <Modal
            {...props}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header closeButton onClick={props.onHide}>
                <Modal.Title id="contained-modal-title-vcenter">
                    Login
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <LoginPopup />
            </Modal.Body>
        </Modal>
    );
}

function RegisterModal(props) {
    return (
        <Modal
            {...props}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header closeButton onClick={props.onHide}>
                <Modal.Title id="contained-modal-title-vcenter">
                    Login
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <RegisterPopup />
            </Modal.Body>
        </Modal>
    );
}