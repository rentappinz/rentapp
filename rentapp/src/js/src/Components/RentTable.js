import React, { Component } from "react";
import Table from "react-bootstrap/Table";
import { Row, Col, Button, Modal } from "react-bootstrap";
import Form from "react-bootstrap/Form";
import 'reactjs-popup/dist/index.css';
import axios from "axios";
import UpdateVehiclePopup from "./UpdateVehiclePopup";
import UpdateRentPopup from "./UpdateRentPopup";

export default class RentTable extends Component {
    constructor() {
        super();
        this.state = {
            modalShow: false,
            rentList: [],
            make: "",
            model: "",
            registration: "",
            minPrice: 0,
            maxPrice: 1000000000,
            status: "",
            startDate: "",
            endDate: "",
            rentToUpdate: null,
        };
        axios.defaults.baseURL = 'http://localhost:8080';
        this.handleChange = this.handleChange.bind(this)
    }
    handleChange(event) {
        const { name, value } = event.target;
        alert(value)
        this.setState({ [name]: value });
    }
    componentDidMount() {
        this.search()
    }

    setModalShow(value, rent) {
        if (rent.rentStatus === "RENTED" || rent.rentStatus === "BOOKED") {
            this.setState({ modalShow: value })
            this.setState({ rentToUpdate: rent })
        }
    }

    search() {

        const config = {
            headers: {
                'Authorization': 'Bearer ' + window.sessionStorage.getItem("jwt")
              },
            proxy: {
                host: "localhost",
                port: 8080,
            }
        };

        axios
            .post("/api/rent/search",
                {
                }
                , config
            )
            .then((res) => {
                this.setState({ rentList: res.data });
            })
            .catch((e) => {
                alert(e)
            });
    }


    render() {
        return (<>
        
            <br />
            <Table bordered hover variant="dark" style={{ textAlign: "center" }}>
                <thead>
                    <tr>
                        <th style={{ width: "5%" }}>#</th>
                        <th style={{ width: "15%" }}>User</th>
                        <th style={{ width: "15%" }}>Vehicle</th>
                        <th style={{ width: "15%" }}>Start Date</th>
                        <th style={{ width: "15%" }}>End Date</th>
                        <th style={{ width: "15%" }}>Rent Status</th>
                        <th style={{ width: "15%" }}>Return Location</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        (this.state.rentList
                            .map((rent, i) => (
                                <tr key={i}
                                    onClick={() => this.setModalShow(true, rent)}
                                >
                                    <td className="align-middle">{rent.id}</td>
                                    <td>User: {rent.appUser.id}<br />{rent.appUser.email}<br />{rent.appUser.firstName}  {rent.appUser.lastName}</td>
                                    <td>Vehicle: {rent.vehicle.id}<br />{rent.vehicle.make} {rent.vehicle.model}<br />{rent.vehicle.registration}</td>
                                    <td>{rent.startDate}</td>
                                    <td>{rent.endDate}</td>
                                    <td>{rent.rentStatus}</td>
                                    <td>Location: {rent.returnLocation.id}<br />{rent.returnLocation.address.city}<br />{rent.returnLocation.address.street} {rent.returnLocation.address.houseNumber}</td>
                                </tr>
                            )))
                    }
                </tbody>
            </Table>
            <MyPopup
                show={this.state.modalShow}
                onHide={() => this.setModalShow(false,this.state.rentToUpdate)}
                rent={this.state.rentToUpdate}

            />
        </>
        )
    }

}


function MyPopup(props) {
    return (

        <Modal
            {...props}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header closeButton onClick={props.onHide}>
                <Modal.Title id="contained-modal-title-vcenter">
                    Update rent
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <UpdateRentPopup rent={props.rent} />
            </Modal.Body>
        </Modal>
    );
}