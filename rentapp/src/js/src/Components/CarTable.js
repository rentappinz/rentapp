import React, { Component } from "react";
import Table from "react-bootstrap/Table";
import { Row, Col, Button, Modal } from "react-bootstrap";
import Form from "react-bootstrap/Form";
import 'reactjs-popup/dist/index.css';
import axios from "axios";
import UpdateVehiclePopup from "./UpdateVehiclePopup";
import CreateRentPopup from "./CreateRentPopup";

export default class CarTable extends Component {
  constructor() {
    super();
    this.state = {
      modalShow: false,
      modalShowRent: false,
      vehicleList: [],
      make: "",
      model: "",
      registration: "",
      minPrice: 0,
      maxPrice: 1000000000,
      status: "",
      startDate: "",
      endDate: "",
      vehicleToUpdate: null,
      vehicleToRent: null,
    };
    axios.defaults.baseURL = 'http://localhost:8080';
    this.handleChange = this.handleChange.bind(this)
  }
  handleChange(event) {
    const { name, value } = event.target;
    this.setState({ [name]: value });
  }
  componentDidMount() {
    this.search()
  }

  setModalShow(value, vehicle) {
    if(window.sessionStorage.getItem("jwt")!=null){
    if(window.sessionStorage.getItem("jwt").includes("ADMIN") ||window.sessionStorage.getItem("jwt").includes("EMPLOYEE")){
    this.setState({ modalShow: value })
    this.setState({ vehicleToUpdate: vehicle })
    }
  }
  }

  setModalShowRent(value, vehicle) {
    this.setState({ modalShowRent: value })
    this.setState({ vehicleToRent: vehicle })
  }

  renderRemovingButton(element, vehicle) {
    if(window.sessionStorage.getItem("jwt")!=null){
      if (element === "th") {
        return <th>Rent</th>;
      }
      if (element === "td") {
        
        return (
          <td onclick="event.cancelBubble=true; return false;">
            <Button
              variant="warning"
              onClick={() => {
             this.setModalShowRent(true,vehicle)
              }}
            >
              +
            </Button>
          </td>
        );
      }
    }

    return null;
  }

  search() {

    const config = {
      proxy: {
        host: "localhost",
        port: 8080,
      }
    };

    axios
      .post("/api/vehicle/search",
        {
          make: this.state.make,
          model: this.state.model,
          registration: this.state.registration,
          minPrice: this.state.minPrice,
          maxPrice: this.state.maxPrice,
          status: this.state.status
        }
        , config
      )
      .then((res) => {
        this.setState({ vehicleList: res.data });
      })
      .catch((e) => {
        alert(e)
      });
  }


  render() {
    return (<>
      <Form>
        <h3>Search vehicle</h3>
        <Row>
          <Col>
            <Form.Control type="text" placeholder="Make" name="make" onChange={this.handleChange} />
          </Col>
          <Col>
            <Form.Control placeholder="Model" name="model" onChange={this.handleChange} />
          </Col>
          <Col>
            <Form.Control placeholder="Registration" name="registration" onChange={this.handleChange} />
          </Col>
        </Row>
        <br />
        <Row>
          <Col>
            <Form.Control placeholder="Price/day Min" name="minPrice" onChange={this.handleChange} />
          </Col>
          <Col>
            <Form.Control placeholder="Price/day Max" name="maxPrice" onChange={this.handleChange} />
          </Col>
          <Col>
            <Form.Control as="select" name="status" onChange={this.handleChange} custom>
              <option value="">Status</option>
              <option value="ACTIVE">ACTIVE</option>
              <option value="INACTIVE">INACTIVE</option>
            </Form.Control>
          </Col>
          <Col>
            <Form.Control type="date" placeholder="Start date" name="startDate" onChange={this.handleChange} />
          </Col>
          <Col>
            <Form.Control type="date" placeholder="End date" name="endDate" onChange={this.handleChange} />
          </Col>
        </Row>
        <br />
        <Button variant="primary" block type="submit"
          onClick={(e) => {
            this.search();
            e.preventDefault();
          }}
        >
          Search
        </Button>
      </Form>
      <br />
      <Table bordered hover variant="dark" style={{ textAlign: "center" }}>
        <thead>
          <tr>
            <th style={{ width: "5%" }}>#</th>
            <th style={{ width: "15%" }}>Make</th>
            <th style={{ width: "15%" }}>Model</th>
            <th style={{ width: "15%" }}>Registration</th>
            <th style={{ width: "15%" }}>Price / Day</th>
            <th style={{ width: "15%" }}>Mileage</th>
            {this.renderRemovingButton("th", null)}
          </tr>
        </thead>
        <tbody>
          {
            (this.state.vehicleList
              .map((vehicle, i) => (
                <tr key={i} >
                  <td className="align-middle" onClick={() => this.setModalShow(true, vehicle)}>{vehicle.id}</td>
                  <td onClick={() => this.setModalShow(true, vehicle)}>{vehicle.make}</td>
                  <td onClick={() => this.setModalShow(true, vehicle)}>{vehicle.model}</td>
                  <td onClick={() => this.setModalShow(true, vehicle)}>{vehicle.registration}</td>
                  <td onClick={() => this.setModalShow(true, vehicle)}>{vehicle.price}</td>
                  <td onClick={() => this.setModalShow(true, vehicle)}>{vehicle.mileage}</td>
                  {this.renderRemovingButton("td", vehicle)}
                </tr>
              )))
          }
        </tbody>
      </Table>
      <MyPopup
        show={this.state.modalShow}
        onHide={() => this.setModalShow(false)}
        vehicle={this.state.vehicleToUpdate}

      />
      <RentVehicle
        show={this.state.modalShowRent}
        onHide={() => this.setModalShowRent(false)}
        vehicle={this.state.vehicleToRent}
        startDate={this.state.startDate}
        endDate={this.state.endDate}

      />
    </>
    )
  }

}


function MyPopup(props) {
  return (

    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton onClick={props.onHide}>
        <Modal.Title id="contained-modal-title-vcenter">
          Update vehicle
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <UpdateVehiclePopup vehicle={props.vehicle} />
      </Modal.Body>
    </Modal>
  );
}

function RentVehicle(props) {
  return (

    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton onClick={props.onHide}>
        <Modal.Title id="contained-modal-title-vcenter">
          Rent vehicle
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <CreateRentPopup vehicle={props.vehicle}  startDate={props.startDate} endDate={props.endDate} />
      </Modal.Body>
    </Modal>
  );
}