import React, { Component } from "react";
import Table from "react-bootstrap/Table";
import axios from "axios";
import { Row, Col, Button, Modal } from "react-bootstrap";
import Form from "react-bootstrap/Form";
import 'reactjs-popup/dist/index.css';
import UpdateLocationPopup from "./UpdateLocationPopup";

export default class LocationTable extends Component {
    constructor() {
        super();
        this.state = {
            modalShow: false,
            locationToUpdate:null,
            locationList: [],
        };
        axios.defaults.baseURL = 'http://localhost:8080';
    }

    componentDidMount() {
        const config = {
            // headers: {
            //     'Authorization': 'Bearer ' + window.sessionStorage.getItem("jwt")
            //   },
            proxy: {
                host: "localhost",
                port: 8080,
            }
        };
        axios
            .get("/api/location", config)
            .then((res) => {
                this.setState({ locationList: res.data });
                console.log(res.data);
            })
            .catch((e) => {
                alert(e)
            });
    }

    setModalShow(value, location) {
        this.setState({ modalShow: value })
        this.setState({ locationToUpdate: location })
      }


    render() {
        return (
            <>
            <Table bordered hover variant="dark" style={{ textAlign: "center" }}>
                <thead>
                    <tr>
                        <th style={{ width: "5%" }}>#</th>
                        <th style={{ width: "15%" }}>Address</th>
                        <th style={{ width: "15%" }}>Status</th>
                        {/* {this.renderRemovingButton("th", null)} */}
                    </tr>
                </thead>
                <tbody>
                    {
                        (this.state.locationList
                            .map((location, i) => (
                                <tr key={i} onClick={() => this.setModalShow(true, location)}>
                                    <td className="align-middle">{location.id}</td>
                                    <td>{location.address.city + ", " + location.address.street + " " + location.address.houseNumber}</td>
                                    <td>{location.status}</td>
                                </tr>
                            )))
                    }
                </tbody>
            </Table>
            <MyPopup
            show={this.state.modalShow}
            onHide={() => this.setModalShow(false)}
            location={this.state.locationToUpdate}
    
          />
          </>
        )
    }

}

function MyPopup(props) {
    return (
  
      <Modal
        {...props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton onClick={props.onHide}>
          <Modal.Title id="contained-modal-title-vcenter">
            Update vehicle
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <UpdateLocationPopup location={props.location} />
        </Modal.Body>
      </Modal>
    );
  }