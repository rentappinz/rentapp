import React, { Component } from "react";
import { Row, Col, Button } from "react-bootstrap";
import axios from "axios";
import Form from "react-bootstrap/Form";

export default class UpdateRentPopup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            rent: props.rent,
            vehicleId: props.rent.vehicle.id,
            userId: props.rent.appUser.id,
            startDate: props.rent.startDate,
            endDate: props.rent.endDate,
            returnLocation: props.rent.returnLocation.id,
            rentStatus: props.rent.rentStatus,
            locations: [],
        }
        axios.defaults.baseURL = 'http://localhost:8080';
        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount() {
        //pobrac id usera z sesion storage i ustawic na user
        const config = {
            headers: {
                'Authorization': 'Bearer ' + window.sessionStorage.getItem("jwt")
              },
            proxy: {
                host: "localhost",
                port: 8080,
            }
        };
        axios
            .get("/api/location", config
            ).then((res) => {
                console.log(res.data)
                this.setState({ locations: res.data })
            })
            .catch((e) => {
                alert(e)
            });
    }

    handleChange(event) {
        const { name, value } = event.target;
        this.setState({ [name]: value });
    }

    renderUser() {
        if(window.sessionStorage.getItem("role").includes("ADMIN") || window.sessionStorage.getItem("role").includes("EMPLOYEE") ){
        
        return (
            <Col>
                <Form.Control  type="number" placeholder="UserId" name="userId" value={this.state.userId} onChange={this.handleChange} />
            </Col>
        )
        }else{
            return (
                <Col>
                    <Form.Control disabled type="number" placeholder="UserId" name="userId" value={this.state.userId} onChange={this.handleChange} />
                </Col>
            )
        }
    }

    renderStatus() {
        if(window.sessionStorage.getItem("role").includes("ADMIN") || window.sessionStorage.getItem("role").includes("EMPLOYEE") ){
        
        return (
            <Col>
                    
                        <Form.Control as="select" name="rentStatus" value={this.state.rentStatus} onChange={this.handleChange} custom>
                            <option value="BOOKED">BOOKED</option>
                            <option value="RENTED">RENTED</option>
                            <option value="FINISHED">FINISHED</option>
                            <option value="CANCELED">CANCELED</option>
                        </Form.Control>
                    </Col>
        )
        }else{
            return (
                <Col>
                    
                        <Form.Control disabled as="select" name="rentStatus" value={this.state.rentStatus} onChange={this.handleChange} custom>
                            <option value="BOOKED">BOOKED</option>
                            <option value="RENTED">RENTED</option>
                            <option value="FINISHED">FINISHED</option>
                            <option value="CANCELED">CANCELED</option>
                        </Form.Control>
                    </Col>
            )
        }
    }

    updateRent() {
        const config = {
            headers: {
                'Authorization': 'Bearer ' + window.sessionStorage.getItem("jwt")
              },
            proxy: {
                host: "localhost",
                port: 8080,
            }
        };
        axios
            .put("/api/rent/"+this.state.rent.id,
                {
                    appUser: {
                        id: this.state.userId
                    },
                    vehicle: {
                        id: this.state.vehicleId
                    },
                    startDate: this.state.startDate,
                    endDate: this.state.endDate,
                    rentStatus: this.state.rentStatus,
                    returnLocation: {
                        id: this.state.returnLocation
                    }
                },
                config
            ).then((res) => {
                window.location.reload();
            })
            .catch((e) => {
                alert(e)
                console.log(e)
            });
    }

    render() {
        return (<>
            <Form>
                <Row>
                    User Id
                    {this.renderUser()}
                    <Col>
                        <Form.Control required as="select" name="returnLocation" value={this.state.returnLocation} onChange={this.handleChange} custom>
                            <option value="" >Select return location</option>
                            {this.state.locations.map((location, i) => {
                                return <option value={location.id}>{location.address.city}, {location.address.street} {location.address.houseNumber}</option>
                            })}
                        </Form.Control>
                    </Col>
                </Row>
                <br />
                <Row>
                    <Col>
                        <Form.Control type="number" name="vehicleId" value={this.state.vehicleId} onChange={this.handleChange} />
                    </Col>
                {this.renderStatus()}
                </Row>
                <br />
                <Row>
                    Start Date
                    <Col>
                        <Form.Control type="date" name="startDate" value={this.state.startDate} onChange={this.handleChange} />
                    </Col>
                    End Date
                    <Col>
                        <Form.Control type="date" name="endDate" value={this.state.endDate} onChange={this.handleChange} />
                    </Col>
                </Row>
                <br />

                <Button variant="primary" block type="submit"
                    onClick={(e) => {
                        this.updateRent();
                        e.preventDefault();
                    }}
                >
                    Update rent
                </Button>
            </Form>
            <br />
        </>
        )
    }
}