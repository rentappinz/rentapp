import React, { Component } from "react";
import Table from "react-bootstrap/Table";
import axios from "axios";
import { Row, Col, Button, Modal } from "react-bootstrap";
import Form from "react-bootstrap/Form";
import 'reactjs-popup/dist/index.css';
import UpdateUserPopup from "./UpdateUserPopup";

export default class UsersTable extends Component {
    constructor() {
        super();
        this.state = {
            modalShow: false,
            userToUpdate: {},
            usersList: [],
            email: "",
            firstName: "",
            lastName: "",
            userRole: "",
            status: "",
            city: "",
            popup: false,
        };
        axios.defaults.baseURL = 'http://localhost:8080';
        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount() {
        this.search()
    }
    setModalShow(value, user) {
        this.setState({ modalShow: value })
        this.setState({ userToUpdate: user })
    }
   
    handleChange(event) {
        const { name, value } = event.target;
        this.setState({ [name]: value });
    }

    search() {
        const config = {
            headers: {
                'Authorization': 'Bearer ' + window.sessionStorage.getItem("jwt")
              },
            proxy: {
                host: "localhost",
                port: 8080,
            }
        };
        axios
            .post("/api/user/search",
                {
                    email: this.state.email,
                    firstName: this.state.firstName,
                    lastName: this.state.lastName,
                    userRole: this.state.userRole,
                    status: this.state.status,
                    city : this.state.city,
                },
                config
            )
            .then((res) => {
                this.setState({ usersList: res.data });
                this.paginationItems();
            })
            .catch((e) => {
                console.log(e)
            });

    }


    render() {
        return (<div>
            <Form>
                <Row>
                    <Col>
                        <Form.Control type="text" placeholder="Email" name="email" onChange={this.handleChange} />
                    </Col>
                    <Col>
                        <Form.Control type="text" placeholder="First name" name="firstName" onChange={this.handleChange} />
                    </Col>
                    <Col>
                        <Form.Control placeholder="Last name" name="lastName" onChange={this.handleChange} />
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Form.Control as="select" name="userRole" onChange={this.handleChange} custom>
                            <option value="">All Roles</option>
                            <option value="CUSTOMER">CUSTOMER</option>
                            <option value="EMPLOYEE">EMPLOYEE</option>
                            <option value="ADMIN">ADMIN</option>
                        </Form.Control>
                    </Col>
                    <Col>
                        <Form.Control as="select" name="status" onChange={this.handleChange} custom>
                            <option value="">All statuses</option>
                            <option value="ACTIVE">ACTIVE</option>
                            <option value="INACTIVE">INACTIVE</option>
                        </Form.Control>
                    </Col>
                    <Col>
                        <Form.Control placeholder="City" name="city" onChange={this.handleChange} />
                    </Col>
                </Row>

                <br />
                <Button variant="primary" block type="submit"
                    onClick={(e) => {
                        this.search();
                        e.preventDefault();
                    }}
                >
                    Search
                </Button>
            </Form>
            <br />
            <Table bordered hover variant="dark" style={{ textAlign: "center" }} >
                <thead>
                    <tr>
                        <th style={{ width: "5%" }}>#</th>
                        <th style={{ width: "15%" }}>First Name</th>
                        <th style={{ width: "15%" }}>Last Name</th>
                        <th style={{ width: "15%" }}>Email</th>
                        <th style={{ width: "15%" }}>Role</th>
                        <th style={{ width: "15%" }}>Address</th>
                        <th style={{ width: "15%" }}>Status</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        (this.state.usersList
                            .map((user, i) => (
                                <tr key={i} onClick={() => this.setModalShow(true, user)}>
                                    <td className="align-middle">{user.id}</td>
                                    <td>{user.firstName}</td>
                                    <td>{user.lastName}</td>
                                    <td>{user.email}</td>
                                    <td>{user.userRole}</td>
                                    <td>{user.address.city + ", " + user.address.street + " " + user.address.houseNumber}</td>
                                    <td>{user.status}</td>
                                </tr>
                            )))
                    }
                </tbody>
            </Table>
            <MyPopup
                show={this.state.modalShow}
                onHide={() => this.setModalShow(false)}
                user={this.state.userToUpdate}

            />
        </div>
        )
    }

}

function MyPopup(props) {
    return (

        <Modal
            {...props}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header closeButton onClick={props.onHide}>
                <Modal.Title id="contained-modal-title-vcenter">
                    Update user
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <UpdateUserPopup user={props.user} />
            </Modal.Body>
        </Modal>
    );
}