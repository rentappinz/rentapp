import React, { Component } from "react";
import { Row, Col, Button } from "react-bootstrap";
import axios from "axios";
import Form from "react-bootstrap/Form";

export default class UpdateLocationPopup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            vehicle: props.vehicle,
            userId: null,
            startDate: props.startDate,
            endDate: props.endDate,
            returnLocation: null,
            locations: [],
        }
        axios.defaults.baseURL = 'http://localhost:8080';
        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount() {
        this.setState({userId:window.sessionStorage.getItem("userId")})
        const config = {
            proxy: {
                host: "localhost",
                port: 8080,
            }
        };
        axios
            .get("/api/location", config
            ).then((res) => {
                console.log(res.data)
                this.setState({ locations: res.data })
            })
            .catch((e) => {
                alert(e)
            });
    }

    handleChange(event) {
        const { name, value } = event.target;
        this.setState({ [name]: value });
    }

    

    renderUser() {
        // if(role == customer) {to zablokowany na jego numerze}
        // else nie zablokowana i mozna zmienic
        return (
            <Col>
                <Form.Control disabled type="number" placeholder="UserId" name="userId" value={this.state.userId} onChange={this.handleChange} />
            </Col>
        )
    }

    createRent() {
        const config = {
            headers: {
                'Authorization': 'Bearer ' + window.sessionStorage.getItem("jwt")
              },
            proxy: {
                host: "localhost",
                port: 8080,
            }
        };
        axios
            .post("/api/rent",
                {
                    appUser: {
                        id: this.state.userId
                    },
                    vehicle: {
                        id: this.state.vehicle.id
                    },
                    startDate: this.state.startDate,
                    endDate: this.state.endDate,
                    rentStatus: "BOOKED",
                    returnLocation: {
                        id: this.state.returnLocation
                    }
                },
                config
            ).then((res) => {
                window.location.reload();
            })
            .catch((e) => {
                alert(e)
            });
    }

    render() {
        return (<>
            <Form>
                <Row>
                    User Id
                    {this.renderUser()}
                    <Col>
                        <Form.Control required as="select" name="returnLocation" onChange={this.handleChange} custom>
                        <option value="" >Select return location</option>
                        {this.state.locations.map((location,i)=>{
                            return <option value={location.id}>{location.address.city}, {location.address.street} {location.address.street}</option>
                        })}
                        </Form.Control>
                    </Col>
                </Row>
                <br />
                <Row>
                    Start Date
                    <Col>
                        <Form.Control type="date" name="startDate" value={this.state.startDate} onChange={this.handleChange} />
                    </Col>
                    End Date
                    <Col>
                        <Form.Control type="date" name="endDate" value={this.state.endDate} onChange={this.handleChange} />
                    </Col>
                </Row>
                <br />

                <Button variant="primary" block type="submit"
                    onClick={(e) => {
                        this.createRent();
                        e.preventDefault();
                    }}
                >
                    Create rent
                </Button>
            </Form>
            <br />
        </>
        )
    }
}