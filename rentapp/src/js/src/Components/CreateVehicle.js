import React, { Component } from "react";
import { Row, Col, Button } from "react-bootstrap";
import axios from "axios";
import Form from "react-bootstrap/Form";

export default class CreateVehicle extends Component {
    constructor() {
        super();
        this.state = {
            make: "",
            model: "",
            registration: "",
            prive: "",
            mileage: "",
            status: "ACTIVE"
        }
        axios.defaults.baseURL = 'http://localhost:8080';
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        const { name, value } = event.target;
        this.setState({ [name]: value });
    }

    createVehicle() {
        const config = {
            headers: {
                'Authorization': 'Bearer ' + window.sessionStorage.getItem("jwt")
              },
            proxy: {
                host: "localhost",
                port: 8080,
            }
        };
        axios
            .post("/api/vehicle",
                {
                    make: this.state.make,
                    model: this.state.model,
                    registration: this.state.registration,
                    price: this.state.price,
                    mileage: this.state.mileage,
                    status: this.state.status
                },
                config
            )
            .catch((e) => {
                console.log(e)
            });


    }

    render() {
        return (
            <>
                <Form>
                    <h3>Create vehicle</h3>
                    <Row>
                        <Col>
                            <Form.Control type="text" placeholder="Make" name="make" onChange={this.handleChange} />
                        </Col>
                        <Col>
                            <Form.Control placeholder="Model" name="model" onChange={this.handleChange} />
                        </Col>
                        <Col>
                            <Form.Control placeholder="Registration" name="registration" onChange={this.handleChange} />
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col>
                            <Form.Control placeholder="Price/day" name="price" onChange={this.handleChange} />
                        </Col>
                        <Col>
                            <Form.Control placeholder="Mileage" name="mileage" onChange={this.handleChange} />
                        </Col>

                        <Col>
                            <Form.Control as="select" name="status" onChange={this.handleChange} custom>
                                <option value="ACTIVE">ACTIVE</option>
                                <option value="INACTIVE">INACTIVE</option>
                            </Form.Control>
                        </Col>
                    </Row>
                    <br />
                    <Button variant="primary" block type="submit"
                        onClick={(e) => {
                            this.createVehicle();
                        }}
                    >
                        Create vehicle
                    </Button>
                </Form>
                <br />
            </>
        )
    }
}