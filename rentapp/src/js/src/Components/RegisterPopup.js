import React, { Component } from "react";
import { Row, Col, Button } from "react-bootstrap";
import axios from "axios";
import Form from "react-bootstrap/Form";

export default class RegisterPopup extends Component {
    constructor() {
        super();
        this.state = {
            email: "",
            password: "",
            firstName: "",
            lastName: "",
            status: "ACTIVE",
            city: "",
            street: "",
            houseNumber: ""
        }
        axios.defaults.baseURL = 'http://localhost:8080';
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        const { name, value } = event.target;
        this.setState({ [name]: value });
    }

    register() {
        const config = {
            proxy: {
                host: "localhost",
                port: 8080,
            }
        };
        axios
            .post("/api/user",
                {
                    email: this.state.email,
                    password: this.state.password,
                    firstName: this.state.firstName,
                    lastName: this.state.lastName,
                    status: this.state.status,
                    address: {
                        city: this.state.city,
                        street: this.state.street,
                        houseNumber: this.state.houseNumber
                    }
                },
                config
            ).then((res) => {
                alert("User registered")
                window.location.reload()

            })
            .catch((e) => {
               alert(e)
            });


    }

    render() {
        return (
            <Form>
                <Row>
                    <Col>
                        <Form.Control type="text" placeholder="Email" name="email"  onChange={this.handleChange} />
                    </Col>
                    <Col>
                        <Form.Control type="password" placeholder="Password" name="password"  onChange={this.handleChange} />
                    </Col>
                    <Col>
                        <Form.Control type="text" placeholder="First name" name="firstName"  onChange={this.handleChange} />
                    </Col>
                    <Col>
                        <Form.Control placeholder="Last name" name="lastName"  onChange={this.handleChange} />
                    </Col>
                </Row>
                <br/>
                <Row>
                    <p>Address</p>
                    <Col>
                        <Form.Control placeholder="City" name="city"  onChange={this.handleChange} />
                    </Col>
                    <Col>
                        <Form.Control placeholder="Street" name="street"  onChange={this.handleChange} />
                    </Col>
                    <Col>
                        <Form.Control placeholder="House Number" name="houseNumber" onChange={this.handleChange} />
                    </Col>
                </Row>
                <br />
                <Button variant="primary" block type="submit"
                    onClick={(e) => {
                        this.register();
                        e.preventDefault();
                    }}
                >
                    Register
                </Button>
            </Form>
        )
    }
}