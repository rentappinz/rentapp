import React, { Component } from "react";
import { Row, Col, Button } from "react-bootstrap";
import axios from "axios";
import Form from "react-bootstrap/Form";

export default class UpdateVehiclePopup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: props.vehicle.id,
            make: props.vehicle.make,
            model: props.vehicle.model,
            registration: props.vehicle.registration,
            price: props.vehicle.price,
            mileage: props.vehicle.mileage,
            status: props.vehicle.status
        }
        axios.defaults.baseURL = 'http://localhost:8080';
        this.handleChange = this.handleChange.bind(this);
        this.updateUser = this.updateVehicle.bind(this);
    }

    handleChange(event) {
        const { name, value } = event.target;
        this.setState({ [name]: value });
    }

    updateVehicle() {
        const config = {
            headers: {
                'Authorization': 'Bearer ' + window.sessionStorage.getItem("jwt")
              },
            proxy: {
                host: "localhost",
                port: 8080,
            }
        };
        alert(this.state.make)
        axios
            .put("/api/vehicle/" + this.state.id,
                {
                    make: this.state.make,
                    model: this.state.model,
                    registration: this.state.registration,
                    price: this.state.price,
                    mileage: this.state.mileage,
                    status: this.state.status
                },
                config
            ).then((res)=>{
            window.location.reload();
            })
            .catch((e) => {
                alert(e)
            });


    }

    render() {
        return (
            <>
            <Form>
                <Row>
                    <Col>
                        <Form.Control type="text" placeholder="Make" value={this.state.make} name="make" onChange={this.handleChange} />
                    </Col>
                    <Col>
                        <Form.Control placeholder="Model" name="model" value={this.state.model}  onChange={this.handleChange} />
                    </Col>
                    <Col>
                        <Form.Control placeholder="Registration" name="registration" value={this.state.registration}  onChange={this.handleChange} />
                    </Col>
                </Row>
                <br />
                <Row>
                    <Col>
                        <Form.Control type="number" placeholder="Price/day" name="price" value={this.state.price}  onChange={this.handleChange} />
                    </Col>
                    <Col>
                        <Form.Control placeholder="Mileage" name="mileage" value={this.state.mileage}  onChange={this.handleChange} />
                    </Col>

                    <Col>
                        <Form.Control as="select" name="status" value={this.state.status}  onChange={this.handleChange} custom>
                            <option value="ACTIVE">ACTIVE</option>
                            <option value="INACTIVE">INACTIVE</option>
                        </Form.Control>
                    </Col>
                </Row>
                <br />
                <Button variant="primary" block type="submit"
                    onClick={(e) => {
                        this.updateVehicle();
                        e.preventDefault();
                    }}
                >
                    Create vehicle
                </Button>
            </Form>
            <br/>
            </>
        )
    }
}