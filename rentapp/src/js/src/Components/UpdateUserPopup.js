import React, { Component } from "react";
import { Row, Col, Button } from "react-bootstrap";
import axios from "axios";
import Form from "react-bootstrap/Form";

export default class UpdateUserPopup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: props.user.id,
            firstName: props.user.firstName,
            lastName: props.user.lastName,
            userRole: props.user.userRole,
            status: props.user.status,
            city: props.user.address.city,
            street: props.user.address.street,
            houseNumber: props.user.address.houseNumber
        }
        axios.defaults.baseURL = 'http://localhost:8080';
        this.handleChange = this.handleChange.bind(this);
        this.updateUser = this.updateUser.bind(this);
    }

    handleChange(event) {
        const { name, value } = event.target;
        this.setState({...this.state.user, [name]: value });
    }

    updateUser() {
        const config = {
            headers: {
                'Authorization': 'Bearer ' + window.sessionStorage.getItem("jwt")
              },
            proxy: {
                host: "localhost",
                port: 8080,
            }
        };
        axios
            .put("/api/user/"+this.state.id,
                {
                    firstName: this.state.firstName,
                    lastName: this.state.lastName,
                    userRole: this.state.userRole,
                    status: this.state.status,
                    address: {
                        city: this.state.city,
                        street: this.state.street,
                        houseNumber: this.state.houseNumber
                    }
                },
                config
            )
            .catch((e) => {
                console.log(e)
            });


    }

    render() {
        return (
            <Form>
                <Row>
                    <Col>
                        <Form.Control type="text" placeholder="First name" name="firstName" value={this.state.firstName} onChange={this.handleChange} />
                    </Col>
                    <Col>
                        <Form.Control placeholder="Last name" name="lastName" value={this.state.lastName} onChange={this.handleChange} />
                    </Col>
                </Row>
                <br/>
                <Row>
                    <Col>
                        <Form.Control placeholder="City" name="city" value={this.state.city} onChange={this.handleChange} />
                    </Col>
                    <Col>
                        <Form.Control placeholder="Street" name="street" value={this.state.street} onChange={this.handleChange} />
                    </Col>
                    <Col>
                        <Form.Control placeholder="House Number" name="houseNumber" value={this.state.houseNumber} onChange={this.handleChange} />
                    </Col>
                </Row>
                <br/>
                <Row>
                    <Col>
                        <Form.Control as="select" name="userRole" value={this.state.userRole} onChange={this.handleChange} custom>
                            <option value="CUSTOMER">CUSTOMER</option>
                            <option value="EMPLOYEE">EMPLOYEE</option>
                            <option value="ADMIN">ADMIN</option>
                        </Form.Control>
                    </Col>
                    <Col>
                        <Form.Control as="select" name="status" value={this.state.status} onChange={this.handleChange} custom>
                            <option value="ACTIVE">ACTIVE</option>
                            <option value="INACTIVE">INACTIVE</option>
                        </Form.Control>
                    </Col>
                </Row>
                <br />
                <Button variant="primary" block type="submit"
                    onClick={(e) => {
                        this.updateUser();
                    }}
                >
                    Update
                </Button>
            </Form>
        )
    }
}