import React, { Component } from "react";
import { Row, Col, Button } from "react-bootstrap";
import axios from "axios";
import Form from "react-bootstrap/Form";

export default class CreateVehicle extends Component {
    constructor() {
        super();
        this.state = {
            city: "",
            street: "",
            houseNumber: "",
            status: "ACTIVE"
        }
        axios.defaults.baseURL = 'http://localhost:8080';
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        const { name, value } = event.target;
        this.setState({ [name]: value });
    }

    createLocation() {
        const config = {
            headers: {
                'Authorization': 'Bearer ' + window.sessionStorage.getItem("jwt")
              },
            proxy: {
                host: "localhost",
                port: 8080,
            }
        };
        axios
            .post("/api/location",
                {
                    address:{
                        city: this.state.city,
                        street: this.state.street,
                        houseNumber: this.state.houseNumber,
                    },
                    status: this.state.status
                },
                config
            )
            .catch((e) => {
                alert(e)
            });


    }

    render() {
        return (<>
            <Form>
                <h3>Create location</h3>
                <Row>
                    <Col>
                        <Form.Control type="text" placeholder="City" name="city" onChange={this.handleChange} />
                    </Col>
                    <Col>
                        <Form.Control placeholder="Street" name="street" onChange={this.handleChange} />
                    </Col>
                </Row>
                <br />
                <Row>
                    <Col>
                        <Form.Control placeholder="House Number" name="houseNumber" onChange={this.handleChange} />
                    </Col>
                    <Col>
                        <Form.Control as="select" name="status" onChange={this.handleChange} custom>
                            <option value="ACTIVE">ACTIVE</option>
                            <option value="INACTIVE">INACTIVE</option>
                        </Form.Control>
                    </Col>
                </Row>
                <br />
                <Button variant="primary" block type="submit"
                    onClick={(e) => {
                        this.createLocation();
                    }}
                    >
                    Create location
                </Button>
            </Form>
                <br/>
                    </>
        )
    }
}