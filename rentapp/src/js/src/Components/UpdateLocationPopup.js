import React, { Component } from "react";
import { Row, Col, Button } from "react-bootstrap";
import axios from "axios";
import Form from "react-bootstrap/Form";

export default class UpdateLocationPopup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: props.location.id,
            city: props.location.address.city,
            street: props.location.address.street,
            houseNumber: props.location.address.houseNumber,
            status: props.location.status
        }
        axios.defaults.baseURL = 'http://localhost:8080';
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        const { name, value } = event.target;
        this.setState({ [name]: value });
    }

    updateLocation() {
        const config = {
            headers: {
                'Authorization': 'Bearer ' + window.sessionStorage.getItem("jwt")
              },
            proxy: {
                host: "localhost",
                port: 8080,
            }
        };
        axios
            .put("/api/location/"+this.state.id,
                {
                    address:{
                        city: this.state.city,
                        street: this.state.street,
                        houseNumber: this.state.houseNumber,
                    },
                    status: this.state.status
                },
                config
            ).then((res)=>{
                window.location.reload();
            })
            .catch((e) => {
                alert(e)
            });


    }

    render() {
        return (<>
            <Form>
                <Row>
                    <Col>
                        <Form.Control type="text" placeholder="City" name="city" value={this.state.city} onChange={this.handleChange} />
                    </Col>
                    <Col>
                        <Form.Control placeholder="Street" name="street" value={this.state.street} onChange={this.handleChange} />
                    </Col>
                </Row>
                <br />
                <Row>
                    <Col>
                        <Form.Control placeholder="House Number" name="houseNumber" value={this.state.houseNumber} onChange={this.handleChange} />
                    </Col>
                    <Col>
                        <Form.Control as="select" name="status" value={this.state.status} onChange={this.handleChange} custom>
                            <option value="ACTIVE">ACTIVE</option>
                            <option value="INACTIVE">INACTIVE</option>
                        </Form.Control>
                    </Col>
                </Row>
                <br />
                <Button variant="primary" block type="submit"
                    onClick={(e) => {
                        this.updateLocation();
                        e.preventDefault();
                    }}
                    >
                    Update location
                </Button>
            </Form>
                <br/>
                    </>
        )
    }
}