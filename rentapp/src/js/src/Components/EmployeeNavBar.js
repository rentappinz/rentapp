import React, { Component } from "react";
import { Navbar } from "react-bootstrap";
import { Nav } from "react-bootstrap"
import { Container } from "react-bootstrap";
import { Button, Modal } from "react-bootstrap";
import LoginPopup from "./LoginPopup";
import history from "../History";

export default class EmployeeNavBar extends Component {
    constructor() {
        super();
        this.state = {
            modalShow: false,
        };
    }

    setModalShow(value) {
        this.setState({ modalShow: value })
    }

    logout() {
        window.sessionStorage.removeItem("email");
        window.sessionStorage.removeItem("jwt");
        window.sessionStorage.removeItem("jwt_refresh");
        window.sessionStorage.removeItem("role");
        history.push("/")
        window.location.reload();

    }

    roleBasedRender() {
        if (window.sessionStorage.getItem("role") != null) {
    
    
          if (window.sessionStorage.getItem("role").includes("ADMIN")) {
            return(
                <Nav.Link href="/panel/locations">Locations</Nav.Link>
            )
          }
          else{
              return(null)
          }
        }
    }

    renderButtons() {
        console.log(window.sessionStorage.getItem("role"))
        if (window.sessionStorage.getItem("email") != null) {
            return (
                <Nav>
                    <Button onClick={() => this.logout()}>
                        Logout
                    </Button>
                </Nav>
            )
        }
        else {
            return (
                <Nav>
                    <Button onClick={() => this.setModalShow(true)}>
                        Login
                    </Button>
                </Nav>
            )
        }
    }

    render() {
        return (
            <>
                <Navbar bg="dark" variant="dark">
                    <Container>
                        <Navbar.Brand href="/panel/rents">CarRental</Navbar.Brand>
                        <Nav className="me-auto">
                            <Nav.Link href="/panel/rents">Rents</Nav.Link>
                            <Nav.Link href="/panel/users">Users</Nav.Link>
                            <Nav.Link href="/panel/vehicles">Vehicles</Nav.Link>
                            <this.roleBasedRender/>
                        </Nav>
                        {this.renderButtons()}
                    </Container>
                </Navbar>
                <MyPopup
                    show={this.state.modalShow}
                    onHide={() => this.setModalShow(false)}
                />
                <br />
            </>
        )
    }
}

function MyPopup(props) {
    return (
        <Modal
            {...props}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header closeButton onClick={props.onHide}>
                <Modal.Title id="contained-modal-title-vcenter">
                    Login
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <LoginPopup />
            </Modal.Body>
        </Modal>
    );
}