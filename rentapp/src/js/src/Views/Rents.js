import React, { Component } from "react";
import EmployeeNavBar from "../Components/EmployeeNavBar";
import RentTable from "../Components/RentTable";
import history from "../History";

export default class Rents extends Component {


  roleBasedRender() {
    if (window.sessionStorage.getItem("role") != null) {

      if (window.sessionStorage.getItem("role").includes("ADMIN") || window.sessionStorage.getItem("role").includes("EMPLOYEE")) {
        return (
          <>
            <EmployeeNavBar />
            <RentTable />
          </>
        );
      } else {
        history.push("/");
        window.location.reload();
      }
    }
    else {
      history.push("/");
      window.location.reload();
    }
  }
  render() {
    return (
      <>
        <this.roleBasedRender />
      </>
    );
  }
}