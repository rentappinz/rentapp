import React, { Component } from "react";
import EmployeeNavBar from "../Components/EmployeeNavBar";
import UsersTable from "../Components/UsersTable";
import history from "../History";

export default class Users extends Component {

  roleBasedRender() {
    if (window.sessionStorage.getItem("role") != null) {

      if (window.sessionStorage.getItem("role").includes("ADMIN") || window.sessionStorage.getItem("role").includes("EMPLOYEE")) {
        return (
          <>
            <EmployeeNavBar />
            <UsersTable />
          </>
        );
      } else {
        history.push("/");
        window.location.reload();
      }
    }
    else {
      history.push("/");
      window.location.reload();
    }
  }

  render() {
    return (
      <>
        <this.roleBasedRender />
      </>
    );
  }
}