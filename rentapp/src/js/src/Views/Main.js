import React, { Component } from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";

import Home from "./Home.js";
import Rents from "./Rents.js";
import Vehicles from "./Vehicles.js";
import Users from "./Users.js";
import Locations from "./Locations.js";
import UserRents from "./UserRents.js";
import UserInfo from "./UserInfo.js";

export default class Main extends Component {
    render() {
        return (
            <Router>
                <Routes>
                    <Route path={"/"} element={<Home/>} />
                    <Route path={"/user-rents"} element={<UserRents/>} />
                    <Route path={"/user-account"} element={<UserInfo/>} />
                    <Route path={"/panel/rents"} element={<Rents />} />
                    <Route path={"/panel/locations"} element={<Locations />} />
                    <Route path={"/panel/vehicles"} element={<Vehicles />} />
                    <Route path={"/panel/users"} element={<Users />} />
                    <Route path={"*"} element={<NoMatch />}/>
                </Routes>
            </Router>
        )
    }
}

function NoMatch() {
    let location = window.location.pathname;
  
    return (
      <div>
        <h3>
          No match for <code>{location}</code>
        </h3>
      </div>
    );
  }