import React, { Component } from "react";

import CarTable from "../Components/CarTable"
import MyNavBar from "../Components/MyNavBar"
class Home extends Component {
  render(){
      return(<div>
          <MyNavBar/>
          <CarTable/>
      </div>)
  }
}
export default Home;