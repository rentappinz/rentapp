import React, { Component } from "react";
import CarTable from "../Components/CarTable";
import CreateVehicle from "../Components/CreateVehicle";
import EmployeeNavBar from "../Components/EmployeeNavBar";


export default class Vehicles extends Component {

  render() {
    return (
  <>
  <EmployeeNavBar/>
  <CreateVehicle/>
  <CarTable/>
  </>
    );
  }
}