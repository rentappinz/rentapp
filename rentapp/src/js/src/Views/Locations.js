import React, { Component } from "react";
import CreateLocation from "../Components/CreateLocation";
import EmployeeNavBar from "../Components/EmployeeNavBar";
import LocationTable from "../Components/LocationTable";
import history from "../History";


export default class Locations extends Component {

  roleBasedRender() {
    if (window.sessionStorage.getItem("role") != null) {


      if (window.sessionStorage.getItem("role").includes("ADMIN")) {
        return (
          <>
            <EmployeeNavBar />
            <CreateLocation />
            <LocationTable />
          </>
        );
      }
      else {
        history.push("/");
        window.location.reload();
      }
    } else {
      history.push("/");
      window.location.reload();
    }
  }

  render() {
    return (
      <>
        <this.roleBasedRender />
      </>
    );
  }
}