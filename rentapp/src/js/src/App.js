import React from "react";
import { Container } from "react-bootstrap";

import "bootstrap/dist/css/bootstrap.min.css";

import Main from "./Views/Main";
import "./App.css";

function App() {
  return (
    <Container>
      <Main />
    </Container>
  );
}

export default App;
