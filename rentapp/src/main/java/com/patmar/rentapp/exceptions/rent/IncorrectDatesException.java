package com.patmar.rentapp.exceptions.rent;

import com.patmar.rentapp.exceptions.CustomException;
import com.patmar.rentapp.rent.Rent;
import org.springframework.http.HttpStatus;

import java.util.Map;

public class IncorrectDatesException extends CustomException {
    private static final String ERROR_CODE = "E003";

    public IncorrectDatesException(Rent rent) {
        super(String.format("Incorrect dates selected for reservation:", rent.toString()));
        this.errorCode = ERROR_CODE;
        this.objects = Map.of("Rent", rent);
        this.httpStatus = HttpStatus.BAD_REQUEST;
    }
}
