package com.patmar.rentapp.exceptions;

import org.springframework.http.HttpStatus;

import java.util.Map;

/**
 * @author Patryk Markowski
 */
public class ObjectNotFoundInDbException extends CustomException {

    private static final String ERROR_CODE = "E001";

    public ObjectNotFoundInDbException(Object object){
        super(String.format(object.toString()));
        this.errorCode=ERROR_CODE;
        this.objects= Map.of("object",object);
        this.httpStatus = HttpStatus.BAD_REQUEST;
    }
}
