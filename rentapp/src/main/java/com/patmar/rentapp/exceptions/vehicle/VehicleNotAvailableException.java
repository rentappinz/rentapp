package com.patmar.rentapp.exceptions.vehicle;

import com.patmar.rentapp.exceptions.CustomException;
import com.patmar.rentapp.rent.Rent;
import org.springframework.http.HttpStatus;

import java.util.Map;

public class VehicleNotAvailableException extends CustomException {
    private static final String ERROR_CODE = "E002";

    public VehicleNotAvailableException(Rent rent) {
        super(String.format("Vehicle with id %d is not available!", rent.getVehicle().getId()));
        this.errorCode = ERROR_CODE;
        this.objects = Map.of("Rent", rent);
        this.httpStatus = HttpStatus.BAD_REQUEST;
    }
}
