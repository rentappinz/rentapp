package com.patmar.rentapp.exceptions.rent;

import com.patmar.rentapp.exceptions.CustomException;
import com.patmar.rentapp.rent.Rent;
import org.springframework.http.HttpStatus;

import java.util.Map;

public class IncorrectRentStatusChangeException extends CustomException {
    private static final String ERROR_CODE = "E003";

    public IncorrectRentStatusChangeException(Rent rent) {
        super(String.format("Incorrect status change for rent :", rent.toString()));
        this.errorCode = ERROR_CODE;
        this.objects = Map.of("Rent", rent);
        this.httpStatus = HttpStatus.BAD_REQUEST;
    }
}
