package com.patmar.rentapp.exceptions;

import org.springframework.http.HttpStatus;

import java.util.Map;

/**
 * @author Patryk Markowski
 */
public class DeactivationImpossibleOngoingRentsException extends CustomException {

    private static final String ERROR_CODE = "E001";

    public DeactivationImpossibleOngoingRentsException(Object object){
        super("Deactivation impossible ongoing rents for: " + object.toString());
        this.errorCode=ERROR_CODE;
        this.objects= Map.of("object",object);
        this.httpStatus = HttpStatus.BAD_REQUEST;
    }
}
