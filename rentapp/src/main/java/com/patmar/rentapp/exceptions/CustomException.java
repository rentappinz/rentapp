package com.patmar.rentapp.exceptions;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.util.Map;

/**
 * @author Patryk Markowski
 */
@NoArgsConstructor
@Getter
@Setter
public class CustomException extends RuntimeException{

    protected String errorCode;
    protected Map<Object,Object> objects;
    protected HttpStatus httpStatus;

    public CustomException(String message) {
        super(message);
    }
}
