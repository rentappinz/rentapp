package com.patmar.rentapp.common;

import lombok.*;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Embeddable
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Address {

    @NotNull
    @Size(min = 2, message = "City name should be longer than 2 characters!")
    private String city;
    @NotNull
    @Size(min = 2, message = "Street name should be longer than 2 characters!")
    private String street;
    @NotNull
    @Size(min = 1, message = "House number should be longer than 1 characters!")
    private String houseNumber;
}
