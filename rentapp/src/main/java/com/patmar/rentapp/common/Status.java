package com.patmar.rentapp.common;

/**
 * @author Patryk Markowski
 */
public enum Status {
    ACTIVE, INACTIVE
}
