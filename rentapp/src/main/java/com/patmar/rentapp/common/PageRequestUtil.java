package com.patmar.rentapp.common;

import org.springframework.data.domain.PageRequest;

import java.util.Objects;

public class PageRequestUtil {

    public static PageRequest createPageRequest(PageSearchInfo searchRequest) {
        int pageNumber;
        Integer size = 20;
        if (Objects.isNull(searchRequest.getPageNumber()) || searchRequest.getPageNumber() < 1) {
            pageNumber = 0;
        } else {
            pageNumber = searchRequest.getPageNumber() - 1;
        }
        if (searchRequest.getPageSize() != null) {
            size = searchRequest.getPageSize();
        }
        return PageRequest.of(pageNumber, size);
    }
}
