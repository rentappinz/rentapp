package com.patmar.rentapp.common;

import lombok.Data;

@Data
public class PageSearchInfo {

    private Integer pageNumber;
    private Integer pageSize;
}
