package com.patmar.rentapp.location;

import com.patmar.rentapp.common.Address;
import com.patmar.rentapp.common.Status;
import com.patmar.rentapp.vehicle.Vehicle;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * @author Patryk Markowski
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Location {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;
    @Embedded
    private Address address;
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private Status status;
    @OneToMany
    private List<Vehicle> vehicles;
}