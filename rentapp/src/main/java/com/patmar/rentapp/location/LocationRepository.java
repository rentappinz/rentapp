package com.patmar.rentapp.location;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Patryk Markowski
 */
public interface LocationRepository extends JpaRepository<Location,Long> {
}
