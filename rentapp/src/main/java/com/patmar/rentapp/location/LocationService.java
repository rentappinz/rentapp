package com.patmar.rentapp.location;

import java.util.List;

/**
 * @author Patryk Markowski
 */
public interface LocationService {

    Location createLocation(Location location);
    List<Location> getAllLocations();
    Location findLocationById(Long id);
    Location updateLocation(Long id, Location newLocation);
}
