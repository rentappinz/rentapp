package com.patmar.rentapp.location;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author Patryk Markowski
 */
@RestController
@RequiredArgsConstructor
@CrossOrigin("*")
@RequestMapping("/api/location")
public class LocationController {

    private final LocationService locationService;

    @PostMapping
    public ResponseEntity<Location> createLocation(@RequestBody @Valid Location location) {
        return ResponseEntity.ok(locationService.createLocation(location));
    }

    @GetMapping
    public ResponseEntity<List<Location>> getAllLocations() {
        return ResponseEntity.ok(locationService.getAllLocations());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Location> findLocationById(@PathVariable Long id) {
        return ResponseEntity.ok(locationService.findLocationById(id));
    }

    @PutMapping("/{id}")
    public ResponseEntity<Location> updateLocation(@PathVariable Long id, @RequestBody @Valid Location newLocation) {
        return ResponseEntity.ok(locationService.updateLocation(id, newLocation));
    }
}
