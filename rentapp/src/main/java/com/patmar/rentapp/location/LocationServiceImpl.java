package com.patmar.rentapp.location;

import com.patmar.rentapp.exceptions.ObjectNotFoundInDbException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Patryk Markowski
 */
@Service
@RequiredArgsConstructor
public class LocationServiceImpl implements LocationService {

    private final LocationRepository locationRepository;

    @Override
    public Location createLocation(Location location) {
        return locationRepository.save(location);
    }

    @Override
    public List<Location> getAllLocations() {
        return locationRepository.findAll();
    }

    @Override
    public Location findLocationById(Long id) {
        return locationRepository.findById(id)
                .orElseThrow(() -> new ObjectNotFoundInDbException(String.format("Location with id: %s not found",id)));
    }

    @Override
    public Location updateLocation(Long id, Location newLocation) {
        Location oldLocation = findLocationById(id);
        oldLocation.setAddress(newLocation.getAddress());
        oldLocation.setStatus(newLocation.getStatus());
        return locationRepository.save(oldLocation);
    }
}
