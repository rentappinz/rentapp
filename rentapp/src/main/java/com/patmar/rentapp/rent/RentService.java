package com.patmar.rentapp.rent;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * @author Patryk Markowski
 */
public interface RentService {
    List<Rent> getAll(RentSearchRequest searchRequest);
    Rent findRentById(Long id);
    Rent createRent(Rent rent);
    Rent updateRent(Long id, Rent rent);
    List<Rent> userRents(Long id);
}
