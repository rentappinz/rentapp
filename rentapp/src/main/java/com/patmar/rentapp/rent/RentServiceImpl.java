package com.patmar.rentapp.rent;

import com.patmar.rentapp.appuser.AppUser;
import com.patmar.rentapp.appuser.UserService;
import com.patmar.rentapp.common.PageRequestUtil;
import com.patmar.rentapp.common.Status;
import com.patmar.rentapp.exceptions.ObjectNotFoundInDbException;
import com.patmar.rentapp.exceptions.rent.IncorrectDatesException;
import com.patmar.rentapp.exceptions.rent.IncorrectRentStatusChangeException;
import com.patmar.rentapp.exceptions.vehicle.VehicleNotAvailableException;
import com.patmar.rentapp.location.Location;
import com.patmar.rentapp.location.LocationService;
import com.patmar.rentapp.vehicle.Vehicle;
import com.patmar.rentapp.vehicle.VehicleService;
import com.querydsl.core.BooleanBuilder;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static com.patmar.rentapp.rent.RentStatus.*;

/**
 * @author Patryk Markowski
 */
@Service
@RequiredArgsConstructor
public class RentServiceImpl implements RentService {
    private final RentRepository rentRepository;
    private final UserService userService;
    private final VehicleService vehicleService;
    private final LocationService locationService;

    @Override
    public List<Rent> getAll(RentSearchRequest searchRequest) {
        return rentRepository.findAll(createSearchPredicate(searchRequest), PageRequestUtil.createPageRequest(searchRequest)).stream().toList();
    }

    @Override
    public Rent findRentById(Long id) {
        return rentRepository.findById(id).orElseThrow(() -> new ObjectNotFoundInDbException("Rent not found for id: " + id));
    }

    @Override
    public Rent createRent(Rent rent) {
        setRentDetails(rent);
        validateRent(rent);
        return rentRepository.save(rent);
    }

    @Override
    public Rent updateRent(Long id, Rent rent) {
        Rent rentToUpdate = findRentById(id);
        isReservationStatusChangeOk(rentToUpdate, rent);
        validateUpdate(id, rent);
        updateRentDetails(rent, rentToUpdate);
        return rentRepository.save(rentToUpdate);
    }

    @Override
    public List<Rent> userRents(Long id) {
        return rentRepository.findAllByAppUserId(id);
    }

    private boolean isVehicleAvailable(Rent rent) {
        if (Objects.isNull(rent.getVehicle()) || Status.INACTIVE.equals(rent.getVehicle().getStatus())) return false;
        List<Rent> rentsByVehicle = rentRepository.findAllByVehicleId(rent.getVehicle().getId());
        Optional<Rent> rentOptional = rentsByVehicle.stream()
                .filter(r -> !FINISHED.equals(r.getRentStatus()) || !CANCELED.equals(r.getRentStatus())) //wszyskie aktywne lub zarezerwowane
                .filter(r -> !r.getEndDate().isBefore(rent.getStartDate())) // usuwam wszystkie które kończą się przed datą startu
                .filter(r -> !r.getStartDate().isAfter(rent.getEndDate())) // usuwam wszystkie które zaczynają się po dacie zakończenia
                .findAny();

        return rentOptional.isEmpty();
    }

    private boolean isVehicleAvailableToUpdate(Long id, Rent rent) {
        if (Objects.isNull(rent.getVehicle()) || Status.INACTIVE.equals(rent.getVehicle().getStatus())) return false;
        List<Rent> rentsByVehicle = rentRepository.findAllByVehicleId(rent.getVehicle().getId());
        Optional<Rent> rentOptional = rentsByVehicle.stream()
                .filter(r -> !FINISHED.equals(r.getRentStatus()) || !CANCELED.equals(r.getRentStatus())) //wszyskie aktywne lub zarezerwowane
                .filter(r -> !r.getEndDate().isBefore(rent.getStartDate())) // usuwam wszystkie które kończą się przed datą startu
                .filter(r -> !r.getStartDate().isAfter(rent.getEndDate())) // usuwam wszystkie które zaczynają się po dacie zakończenia
                .filter(r -> !r.getId().equals(id))  // pomijam aktualne zamówienie
                .findAny();

        return rentOptional.isEmpty();

    }

    private void validateRent(Rent rent) {
        if (!isRentStartDateOk(rent.getStartDate()) || !isEndDateIsAfterStartDateOk(rent.getStartDate(), rent.getEndDate())) {
            throw new IncorrectDatesException(rent);
        }
        if (!isVehicleAvailable(rent)) {
            throw new VehicleNotAvailableException(rent);
        }
    }

    private void validateUpdate(Long id, Rent rent) {

        if (!isRentStartDateOk(rent.getStartDate()) || !isEndDateIsAfterStartDateOk(rent.getStartDate(), rent.getEndDate())) {
            throw new IncorrectDatesException(rent);
        }
        if (!isVehicleAvailableToUpdate(id, rent)) {
            throw new VehicleNotAvailableException(rent);
        }
    }

    private void isReservationStatusChangeOk(Rent actual, Rent maybeNext) {
        System.out.println(FINISHED.equals(actual.getRentStatus()) && !FINISHED.equals(maybeNext.getRentStatus()));
        System.out.println((RENTED.equals(actual.getRentStatus()) && CANCELED.equals(maybeNext.getRentStatus())));
        if (FINISHED.equals(actual.getRentStatus()) && !FINISHED.equals(maybeNext.getRentStatus())
                || (RENTED.equals(actual.getRentStatus()) && CANCELED.equals(maybeNext.getRentStatus()))
        ) {
            throw new IncorrectRentStatusChangeException(maybeNext);
        }
    }


    private boolean isRentStartDateOk(LocalDate startDate) {
        return LocalDate.now().isEqual(startDate) || LocalDate.now().isBefore(startDate);
    }

    private boolean isEndDateIsAfterStartDateOk(LocalDate startDate, LocalDate endDate) {
        return startDate.isBefore(endDate) || startDate.isEqual(endDate);
    }


    private void setRentDetails(Rent rent) {
        Vehicle vehicle = vehicleService.findVehicleById(rent.getVehicle().getId());
        AppUser appUser = userService.findUserById(rent.getAppUser().getId());
        Location returnLocation = locationService.findLocationById(rent.getReturnLocation().getId());
        rent.setAppUser(appUser);
        rent.setVehicle(vehicle);
        rent.setReturnLocation(returnLocation);
    }

    private void updateRentDetails(Rent rent, Rent rentToUpdate) {
        setRentDetails(rent);
        rentToUpdate.setAppUser(rent.getAppUser());
        rentToUpdate.setVehicle(rent.getVehicle());
        rentToUpdate.setReturnLocation(rent.getReturnLocation());
        rentToUpdate.setRentStatus(rent.getRentStatus());
        rentToUpdate.setEndDate(rent.getEndDate());
        rentToUpdate.setStartDate(rent.getStartDate());
    }

    private BooleanBuilder createSearchPredicate(RentSearchRequest searchRequest) {
        BooleanBuilder builder = new BooleanBuilder();
        if (searchRequest.getMake() != null && !searchRequest.getMake().equals("")) {
            builder.and(QRent.rent.vehicle.make.contains(searchRequest.getMake()));
        }
//        if (searchRequest.getModel() != null && !searchRequest.getModel().equals("")) {
//            builder.and(QVehicle.vehicle.model.contains(searchRequest.getModel()));
//        }
//        if (searchRequest.getRegistration() != null && !searchRequest.getRegistration().equals("")) {
//            builder.and(QVehicle.vehicle.registration.contains(searchRequest.getRegistration()));
//        }
//        if (searchRequest.getMinPrice() != null && searchRequest.getMaxPrice() != null
//                && !searchRequest.getMinPrice().equals("") && !searchRequest.getMinPrice().equals("")) {
//            builder.and(QVehicle.vehicle.price.between(searchRequest.getMinPrice(), searchRequest.getMaxPrice()));
//        }
//        if (searchRequest.getStatus() != null) {
//            builder.and(QVehicle.vehicle.status.eq(searchRequest.getStatus()));
//        }
        return builder;
    }
}
