package com.patmar.rentapp.rent;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.List;

/**
 * @author Patryk Markowski
 */
public interface RentRepository extends JpaRepository<Rent, Long>, QuerydslPredicateExecutor<Rent> {
    List<Rent> findAllByVehicleId(Long vehicleId);
    List<Rent> findAllByAppUserId(Long appUserId);
    List<Rent> findAllByRentStatusIn(List<RentStatus> rentStatuses);
}
