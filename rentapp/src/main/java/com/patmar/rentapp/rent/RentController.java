package com.patmar.rentapp.rent;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Patryk Markowski
 */
@RestController
@RequestMapping("/api/rent")
@CrossOrigin("*")
@RequiredArgsConstructor
public class RentController {

    private final RentService rentService;

    @PostMapping("/search")
    public ResponseEntity<List<Rent>> getAll(RentSearchRequest searchRequest) {
        return ResponseEntity.ok(rentService.getAll(searchRequest));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Rent> findRentById(@PathVariable Long id) {
        return ResponseEntity.ok(rentService.findRentById(id));
    }

    @PostMapping
    public ResponseEntity<Rent> createRent(@RequestBody Rent rent) {
        return ResponseEntity.ok(rentService.createRent(rent));
    }

    @PutMapping("/{id}")
    ResponseEntity<Rent> updateRent(@PathVariable Long id, @RequestBody Rent rent) {
        System.out.println("elooooo");
        return ResponseEntity.ok(rentService.updateRent(id, rent));
    }

    @GetMapping("/{id}/user-rents")
    ResponseEntity<List<Rent>> userRents(@PathVariable Long id){
        return ResponseEntity.ok(rentService.userRents(id));
    }
}
