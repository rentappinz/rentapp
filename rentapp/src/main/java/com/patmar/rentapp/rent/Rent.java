package com.patmar.rentapp.rent;

import com.patmar.rentapp.appuser.AppUser;
import com.patmar.rentapp.location.Location;
import com.patmar.rentapp.vehicle.Vehicle;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

/**
 * @author Patryk Markowski
 */
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Rent {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    private AppUser appUser;
    @ManyToOne
    private Vehicle vehicle;
    private LocalDate startDate;
    private LocalDate endDate;
    @Enumerated(value = EnumType.STRING)
    private RentStatus rentStatus;
    @ManyToOne
    private Location returnLocation;
}
