package com.patmar.rentapp.rent;

/**
 * @author Patryk Markowski
 */
public enum RentStatus {
    CANCELED, BOOKED, RENTED, FINISHED
}
