package com.patmar.rentapp.rent;

import com.patmar.rentapp.appuser.AppUser;
import com.patmar.rentapp.common.PageSearchInfo;
import com.patmar.rentapp.location.Location;
import com.patmar.rentapp.vehicle.Vehicle;
import lombok.Data;

import java.time.LocalDate;

@Data
public class RentSearchRequest extends PageSearchInfo {

    private AppUser appUser;
    private String make;
    private LocalDate startDate;
    private LocalDate endDate;
    private RentStatus rentStatus;
    private Location returnLocation;
}
