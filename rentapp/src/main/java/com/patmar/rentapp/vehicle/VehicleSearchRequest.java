package com.patmar.rentapp.vehicle;

import com.patmar.rentapp.common.PageSearchInfo;
import com.patmar.rentapp.common.Status;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
public class VehicleSearchRequest extends PageSearchInfo {

    private String make;
    private String model;
    private String registration;
    private BigDecimal minPrice = new BigDecimal("0");
    private BigDecimal maxPrice = new BigDecimal("10000");
    private String status;
    private LocalDate startDate;
    private LocalDate endDate;
}
