package com.patmar.rentapp.vehicle;

import java.util.List;

/**
 * @author Patryk Markowski
 */
public interface VehicleService {

    List<Vehicle> searchVehicle(VehicleSearchRequest searchRequest);
    Vehicle findVehicleById(Long id);
    Vehicle createVehicle(Vehicle vehicle);
    Vehicle updateVehicle(Long id, Vehicle vehicle);
}
