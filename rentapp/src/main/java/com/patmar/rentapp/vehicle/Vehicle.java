package com.patmar.rentapp.vehicle;

import com.patmar.rentapp.common.Status;
import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * @author Patryk Markowski
 */
@Entity
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Vehicle {
    //TODO validation
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String make;
    private String model;
    @Column(unique = true)
    private String registration;
    private BigDecimal price;
    private Long mileage;
    @Enumerated(value = EnumType.STRING)
    private Status status;

}
