package com.patmar.rentapp.vehicle;

import com.patmar.rentapp.common.Status;
import com.patmar.rentapp.exceptions.DeactivationImpossibleOngoingRentsException;
import com.patmar.rentapp.exceptions.ObjectNotFoundInDbException;
import com.patmar.rentapp.rent.Rent;
import com.patmar.rentapp.rent.RentRepository;
import com.patmar.rentapp.rent.RentStatus;
import com.querydsl.core.BooleanBuilder;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static com.patmar.rentapp.common.PageRequestUtil.createPageRequest;

/**
 * @author Patryk Markowski
 */
@Service
@RequiredArgsConstructor
public class VehicleServiceImpl implements VehicleService {

    private final VehicleRepository vehicleRepository;
    private final RentRepository rentRepository;


    @Override
    public List<Vehicle> searchVehicle(VehicleSearchRequest searchRequest) {
        validateSearchDates(searchRequest);
        return availableVehicles(
                searchRequest.getStartDate()
                , searchRequest.getEndDate()
                , (List<Vehicle>) vehicleRepository.findAll(createSearchPredicate(searchRequest))
        );
    }

    @Override
    public Vehicle findVehicleById(Long id) {
        return vehicleRepository.findById(id)
                .orElseThrow(() -> new ObjectNotFoundInDbException(String.format("Vehicle with id: %s not found", id)));
    }

    @Override
    public Vehicle createVehicle(Vehicle vehicle) {
        return vehicleRepository.save(vehicle);
    }

    @Override
    public Vehicle updateVehicle(Long id, Vehicle vehicle) {
        Vehicle vehicleToUpdate = findVehicleById(id);

        if(vehicle.getStatus() == Status.INACTIVE){
            verifyDeactivation(vehicleToUpdate);
        }

        vehicleToUpdate.setMake(vehicle.getMake());
        vehicleToUpdate.setModel(vehicle.getModel());
        vehicleToUpdate.setMileage(vehicle.getMileage());
        vehicleToUpdate.setRegistration(vehicle.getRegistration());
        vehicleToUpdate.setStatus(vehicle.getStatus());
        vehicleToUpdate.setPrice(vehicle.getPrice());
        return vehicleRepository.save(vehicleToUpdate);
    }

    private void validateSearchDates(VehicleSearchRequest searchRequest) {
        if (Objects.isNull(searchRequest.getStartDate()) || searchRequest.getStartDate().equals("")) {
            searchRequest.setStartDate(LocalDate.now());
        }
        if (Objects.isNull(searchRequest.getEndDate()) || searchRequest.getEndDate().equals("")) {
            searchRequest.setEndDate(LocalDate.now().plusYears(10));
        }
    }

    private List<Vehicle> availableVehicles(LocalDate startDate, LocalDate endDate, List<Vehicle> vehicles) {
        List<Rent> actualRents = rentRepository.findAllByRentStatusIn(List.of(RentStatus.RENTED, RentStatus.BOOKED));
        return vehicles.stream().filter(vehicle -> isVehicleAvailable(actualRents, startDate, endDate, vehicle)).toList();
    }

    private boolean isVehicleAvailable(List<Rent> rents, LocalDate startDate, LocalDate endDate, Vehicle vehicle) {
        Optional<Rent> rentOptional = rents.stream()
                .filter(r -> r.getVehicle().getId().equals(vehicle.getId())) // tylko rozpatrywany pojazd
                .filter(r -> !r.getEndDate().isBefore(startDate)) // usuwam wszystkie które kończą się przed datą startu
                .filter(r -> !r.getStartDate().isAfter(endDate)) // usuwam wszystkie które zaczynają się po dacie zakończenia
                .findAny();

        return rentOptional.isEmpty();
    }

    private void verifyDeactivation( Vehicle vehicle) {
        Optional<Rent> optionalRent = rentRepository.findAllByVehicleId(vehicle.getId()).stream().filter(
                rent -> rent.getRentStatus() != RentStatus.CANCELED || rent.getRentStatus() != RentStatus.FINISHED
        ).findAny();
        if (optionalRent.isPresent()) throw new DeactivationImpossibleOngoingRentsException(vehicle);
    }

    private BooleanBuilder createSearchPredicate(VehicleSearchRequest searchRequest) {
        BooleanBuilder builder = new BooleanBuilder();
        if (searchRequest.getMake() != null && !searchRequest.getMake().equals("")) {
            builder.and(QVehicle.vehicle.make.contains(searchRequest.getMake()));
        }
        if (searchRequest.getModel() != null && !searchRequest.getModel().equals("")) {
            builder.and(QVehicle.vehicle.model.contains(searchRequest.getModel()));
        }
        if (searchRequest.getRegistration() != null && !searchRequest.getRegistration().equals("")) {
            builder.and(QVehicle.vehicle.registration.contains(searchRequest.getRegistration()));
        }
        if (searchRequest.getMinPrice() != null && searchRequest.getMaxPrice() != null
                && !searchRequest.getMinPrice().equals("") && !searchRequest.getMinPrice().equals("")) {
            builder.and(QVehicle.vehicle.price.between(searchRequest.getMinPrice(), searchRequest.getMaxPrice()));
        }
        if (searchRequest.getStatus() != null && !searchRequest.getStatus().equals("")) {
            builder.and(QVehicle.vehicle.status.eq(Status.valueOf(searchRequest.getStatus())));
        }
        return builder;
    }
}