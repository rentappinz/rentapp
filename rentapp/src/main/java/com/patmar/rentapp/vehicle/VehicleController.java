package com.patmar.rentapp.vehicle;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author Patryk Markowski
 */
@RestController
@RequiredArgsConstructor
@CrossOrigin("*")
@RequestMapping("/api/vehicle")
public class VehicleController {

    private final VehicleService vehicleService;

    @PostMapping("/search")
    public ResponseEntity<List<Vehicle>> searchVehicle(@RequestBody VehicleSearchRequest searchRequest){
        return ResponseEntity.ok(vehicleService.searchVehicle(searchRequest));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Vehicle> findVehicleById(@PathVariable Long id){
        return ResponseEntity.ok(vehicleService.findVehicleById(id));
    }

    @PostMapping
    public ResponseEntity<Vehicle> createVehicle(@RequestBody @Valid Vehicle vehicle){
        return ResponseEntity.ok(vehicleService.createVehicle(vehicle));
    }

    @PutMapping("/{id}")
    public ResponseEntity<Vehicle> updateVehicle(@RequestBody @Valid Vehicle vehicle, @PathVariable Long id){
        System.out.println("eloooo");
        return ResponseEntity.ok(vehicleService.updateVehicle(id,vehicle));
    }
}
