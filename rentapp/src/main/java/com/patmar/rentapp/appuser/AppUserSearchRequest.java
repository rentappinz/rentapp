package com.patmar.rentapp.appuser;

import com.patmar.rentapp.common.PageSearchInfo;
import com.patmar.rentapp.common.Status;
import lombok.Data;

@Data
public class AppUserSearchRequest extends PageSearchInfo {

    private String email;
    private String firstName;
    private String lastName;
    private String userRole;
    private String status;
    private String city;
}
