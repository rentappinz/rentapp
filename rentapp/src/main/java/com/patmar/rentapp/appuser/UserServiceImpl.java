package com.patmar.rentapp.appuser;

import com.patmar.rentapp.common.Status;
import com.patmar.rentapp.exceptions.DeactivationImpossibleOngoingRentsException;
import com.patmar.rentapp.exceptions.ObjectNotFoundInDbException;
import com.patmar.rentapp.rent.Rent;
import com.patmar.rentapp.rent.RentRepository;
import com.patmar.rentapp.rent.RentStatus;
import com.querydsl.core.BooleanBuilder;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static com.patmar.rentapp.appuser.UserRole.*;
import static com.patmar.rentapp.common.PageRequestUtil.createPageRequest;

/**
 * @author Patryk Markowski
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final RentRepository rentRepository;
    private final PasswordEncoder passwordEncoder;

    @Value("${app.email.regex}")
    String emailRegex;

    @Override
    public AppUser registerUser(AppUser appUser) {
        appUser.setPassword(passwordEncoder.encode(appUser.getPassword()));
        evaluateRole(appUser);
        return userRepository.save(appUser);
    }

    @Override
    public List<AppUser> searchUsers(AppUserSearchRequest searchRequest) {
        return userRepository.findAll(createSearchPredicate(searchRequest), createPageRequest(searchRequest)).stream().toList();
    }

    @Override
    public AppUser findUserById(Long id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new ObjectNotFoundInDbException(String.format("User with id: %s not found", id)));
    }

    @Override
    public AppUser updateUser(Long id, AppUser appUser) {
        AppUser userToUpdate = findUserById(id);
        if(appUser.getStatus() == Status.INACTIVE){
            validateDeactivation(userToUpdate);
        }
        setUsersNewValues(appUser, userToUpdate);
        return userRepository.save(userToUpdate);
    }

    @Override
    public AppUser userInfo(Authentication auth) {
        return userRepository.findByEmail(auth.getPrincipal().toString());
    }

    private void setUsersNewValues(AppUser appUser, AppUser userToUpdate) {
        userToUpdate.setFirstName(appUser.getFirstName());
        userToUpdate.setLastName(appUser.getLastName());
        userToUpdate.setUserRole(appUser.getUserRole());
        userToUpdate.setStatus(appUser.getStatus());
        userToUpdate.setAddress(appUser.getAddress());
    }

    private AppUser evaluateRole(AppUser appUser) {
        if (appUser.getEmail().endsWith(emailRegex)) {
            if (appUser.getUserRole() != (ADMIN)) {
                appUser.setUserRole(EMPLOYEE);
            }
        } else {
            appUser.setUserRole(CUSTOMER);
        }
        return appUser;
    }

    private void validateDeactivation(AppUser userToUpdate) {
        Optional<Rent> optionalRent = rentRepository.findAllByAppUserId(userToUpdate.getId()).stream().filter(
                rent -> rent.getRentStatus() != RentStatus.CANCELED || rent.getRentStatus() != RentStatus.FINISHED
        ).findAny();
        if (optionalRent.isPresent()) throw new DeactivationImpossibleOngoingRentsException(userToUpdate);
    }

    private BooleanBuilder createSearchPredicate(AppUserSearchRequest searchRequest) {
        log.info(">> Creating search predicate");
        BooleanBuilder builder = new BooleanBuilder();
        if (searchRequest.getFirstName() != null && !searchRequest.getFirstName().equals("")) {
            builder.and(QAppUser.appUser.firstName.contains(searchRequest.getFirstName()));
        }
        if (searchRequest.getEmail() != null && !searchRequest.getEmail().equals("")) {
            builder.and(QAppUser.appUser.email.contains(searchRequest.getEmail()));
        }
        if (searchRequest.getLastName() != null && !searchRequest.getLastName().equals("")) {
            builder.and(QAppUser.appUser.lastName.contains(searchRequest.getLastName()));
        }
        if (searchRequest.getStatus() != null && !searchRequest.getStatus().equals("")) {
            builder.and(QAppUser.appUser.status.eq(Status.valueOf(searchRequest.getStatus())));
        }
        if (searchRequest.getUserRole() != null && !searchRequest.getUserRole().equals("")) {
            builder.and(QAppUser.appUser.userRole.eq(UserRole.valueOf(searchRequest.getUserRole())));
        }
        if (searchRequest.getCity() != null && !searchRequest.getCity().equals("")) {
            builder.and(QAppUser.appUser.address.city.contains(searchRequest.getCity()));
        }
        log.info("<< Search predicate created");
        return builder;
    }
}