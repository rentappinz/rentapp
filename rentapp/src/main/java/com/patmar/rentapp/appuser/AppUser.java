package com.patmar.rentapp.appuser;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.patmar.rentapp.common.Address;
import com.patmar.rentapp.common.Status;
import lombok.*;

import javax.persistence.*;

/**
 * @author Patryk Markowski
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AppUser {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String email;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;
    private String firstName;
    private String lastName;
    @Enumerated(value = EnumType.STRING)
    private UserRole userRole;
    @Enumerated(value = EnumType.STRING)
    private Status status;
    @Embedded
    private Address address;

    @Override
    public String toString() {
        return "AppUser{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", userRole=" + userRole +
                ", status=" + status +
                ", address=" + address +
                '}';
    }
}
