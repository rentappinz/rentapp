package com.patmar.rentapp.appuser;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

/**
 * @author Patryk Markowski
 */
public interface UserRepository extends JpaRepository<AppUser, Long>, QuerydslPredicateExecutor<AppUser> {
    AppUser findByEmail(String email);
}
