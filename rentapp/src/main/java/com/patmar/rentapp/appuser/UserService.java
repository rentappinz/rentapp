package com.patmar.rentapp.appuser;

import org.springframework.security.core.Authentication;

import java.util.List;

/**
 * @author Patryk Markowski
 */
public interface UserService {

    AppUser registerUser(AppUser appUser);
    List<AppUser> searchUsers(AppUserSearchRequest searchRequest);
    AppUser findUserById(Long id);
    AppUser updateUser(Long id, AppUser appUser);
    AppUser userInfo(Authentication auth);
}
