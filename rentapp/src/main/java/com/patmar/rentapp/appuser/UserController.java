package com.patmar.rentapp.appuser;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.http.parser.Authorization;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author Patryk Markowski
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/user")
@CrossOrigin("*")
@Slf4j
public class UserController {

    private final UserService userService;
    private final PasswordEncoder passwordEncoder;

    @PostMapping
    public ResponseEntity<AppUser> registerUser(@Valid @RequestBody AppUser appUser) {
        log.info("> RegisterUser endpoint performed!");
        return ResponseEntity.ok(userService.registerUser(appUser));
    }

    @PostMapping("/search")
    public ResponseEntity<List<AppUser>> searchUsers(@RequestBody AppUserSearchRequest searchRequest) {
        log.info("> searchUser endpoint performed!");
        return ResponseEntity.ok(userService.searchUsers(searchRequest));
    }

    @GetMapping("/{id}")
    public ResponseEntity<AppUser> findUserById(@PathVariable Long id) {
        System.out.println(passwordEncoder.encode("password"));
        log.info("> findUserById endpoint performed!");
        return ResponseEntity.ok(userService.findUserById(id));
    }

    @PutMapping("/{id}")
    public ResponseEntity<AppUser> updateUser(@RequestBody AppUser appUser, @PathVariable Long id) {
        log.info("> updateUser endpoint performed!");
        return ResponseEntity.ok(userService.updateUser(id, appUser));
    }

    @GetMapping("/info")
    public ResponseEntity<AppUser> userInfo(Authentication auth){
        return ResponseEntity.ok(userService.userInfo(auth));
    }
}
