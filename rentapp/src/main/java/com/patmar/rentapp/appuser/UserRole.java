package com.patmar.rentapp.appuser;

/**
 * @author Patryk Markowski
 */
public enum UserRole {
    CUSTOMER, EMPLOYEE, ADMIN
}
