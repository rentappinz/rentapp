insert into vehicle (make,model,registration,price,mileage,status) values ('Ford','Focus','WE847SK',190,120000,'ACTIVE');
insert into vehicle (make,model,registration,price,mileage,status) values ('Ford','Mondeo','WE0O0O0',220,80000,'ACTIVE');
insert into vehicle (make,model,registration,price,mileage,status) values ('Skoda','Octavia','WE764OK',220,110000,'ACTIVE');
insert into vehicle (make,model,registration,price,mileage,status) values ('Skoda','Rapid','WE66378',220,78000,'ACTIVE');
insert into vehicle (make,model,registration,price,mileage,status) values ('Skoda','Fabia','WE536KF',160,120000,'ACTIVE');
insert into vehicle (make,model,registration,price,mileage,status) values ('Mercedes','C200','WE639JJ',260,123233,'ACTIVE');
insert into vehicle (make,model,registration,price,mileage,status) values ('Audi','A5','WE534YU',300,125678,'ACTIVE');