--liquibase formatted sql
--changeset pmarkowski:2
alter table location_vehicles add constraint UK_location_vehicle_unique_vehicle_id unique (vehicles_id);
alter table vehicle add constraint UK_vehicle_unique_registration unique (registration);
alter table location_vehicles add constraint FK_location_vehicle_vehicle_id foreign key (vehicles_id) references vehicle;
alter table location_vehicles add constraint FK_location_vehicle_location_id foreign key (location_id) references location;
alter table rent add constraint FK_rent_app_user_id foreign key (app_user_id) references app_user;
alter table rent add constraint FK_rent_return_location_id foreign key (return_location_id) references location;
alter table rent add constraint FK_rent_vehicle_id foreign key (vehicle_id) references vehicle;